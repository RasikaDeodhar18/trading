package com.hackathon.trading.model;

public enum TradeState {

    CREATED, PROCESSING, FILLED, REJECTED
    
}
